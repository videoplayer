#ifndef AGL_GETPROC_H_
#define AGL_GETPROC_H_

void my_aglEnableMultiThreading(void);
void my_aglSwapInterval(int interval);

#endif /*AGL_GETPROC_H_*/
